# GitLab Group Member Report

Create a report of the billable members of a SaaS group or all members on a self-managed instance, using the GitLab API and GitLab pages.

**NOTE: The tool was rewritten from scratch to leverage a better API querying strategy, running faster and providing more information. If you find any issues, please open a ticket and check out [v1.0.0](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report/-/tree/v1.0.0).**

## Features

- Queries direct user memberships via the [Users/Memberships API](https://docs.gitlab.com/ee/api/users.html#user-memberships) for self-managed and the [Group Members/Billable Users API](https://docs.gitlab.com/ee/api/members.html#list-all-billable-members-of-a-group) for SaaS.
- Queries all groups and projects in order to resolve group sharing (to get indirect membership via shared groups)
- Provides a report containing highest access level and all groups and projects with access level.
- Provides additional user information like "last sign on", "email" and "status". On SaaS, users need to be [Enterprise users](https://docs.gitlab.com/ee/user/enterprise_user/) in order for this data to be available.
- Produces HTML, CSV and JSON
- Deploys the report using GitLab pages

## Configuration

- Export / fork repository.
- **Make it private, to prevent others from seeing your groups and their members.**
- Add a GitLab API token to CI/CD variables named `GIT_TOKEN`. Scope needs to be `read_api`. Make sure it is "masked". For SaaS, a group `OWNER` token is needed. For self-managed, an `ADMIN` token is needed. This token will be used to query the API for group and project members
- Add the group ID you want to report on to CI/CD variables named `GROUP`. This is the SaaS group that will be reported on.
- **Make sure Settings -> Permissions -> Pages is limited to project members.**
- Run the Pipeline to get your report

## Parameters

- `-u`, `--gitlaburl`: Optional URL of the GitLab instance. Defaults to https://gitlab.com/
- `-g`, `--group`: Top-level group to report on. Valid and mandatory only for SaaS (i.e. https://gitlab.com/). Optional on self-managed, as all groups will be reported on.
- `--all`: Retrieve all users, even if non-billable. Defaults to false.
- `--disable_certificate_verification`: Don't verify SSL certificates for https hosts. Useful for self-managed servers with self-signed certificates that fail verification. Defaults to false (i.e. do verify SSL certificates unless this option is explicitly set).

## Example usage

**Query all billable users and their memberships in a .com group**

`python3 group_member_report.py -g $group_path $token`

**Query all billable users and their memberships on a self-managed instance**

`python3 group_member_report.py -u $instance_url $token`

## Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or better propose a Merge Request. This script creates a report of user names and emails (PII), which has the potential to leak personal data. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.
